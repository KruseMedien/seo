
# SEO Addon SallyCMS #

## ab V3 ##
Legt Metainfo Felder "Seitentitel", "Beschreibung" und "Schlüsselwörter" für Artikel an. Diese können zur Layout Klasse des Projekts hinzugefügt werden, indem das Layout aus dem Addon extended wird.

## ab V1 ##
Stellt eine Sitemap (/sitemap.xml) anhand der Struktur dar.