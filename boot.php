<?php
/**
 * Copyright, KruseMedien GmbH
 *
 * This file is released under the terms of the MIT license.
 * You can find the complete text  online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

/**
 * @var $container sly_Container
 */

define('SEO_ROOT_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

$container->getI18N()->appendFile(__DIR__.'/lang');
$container->getClassLoader()->add('', __DIR__.'/lib');

$dispatcher = $container->getDispatcher();

if (sly_Core::isBackend()) {

	// is Backend

} else {

	// is Frontend
	// add /sitemap.xml
	$dispatcher->addListener('SLY_FRONTEND_ROUTER', array('sly_Controller_Frontend_Sitemap', 'addRoutes'));

}