<?php
/**
 * Copyright, KruseMedien GmbH
 *
 * This file is released under the terms of the MIT license.
 * You can find the complete text  online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */


class sly_Controller_Frontend_Sitemap extends sly_Controller_Frontend_Base {

	public function indexAction() {
		$languages = sly_Util_Language::findAll();
		$articles  = self::getAllArticles();

		$output = '<?xml version="1.0" encoding="UTF-8"?>';
		$output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

		foreach ($articles as $item) {
			$article = sly_Util_Article::findById($item['id'], $item['clang']);
			if ($article) {
				$output .= '<url>';
                $output .= '<loc>'.htmlspecialchars(sly_Util_HTTP::getAbsoluteUrl($article->getId(), $item['clang'])).'</loc>';
				$output .= '<lastmod>'.date('Y-m-d', $article->getUpdatedate()).'</lastmod>';
				$output .= '<changefreq>'.self::getFrequency($article->getUpdatedate()).'</changefreq>';

				// more than one language
				if (count($languages) > 1) {
					$locationLanguages = $languages;
					unset($locationLanguages[$article->getCLang()]);

					foreach ($locationLanguages as $language) {
						if (sly_Util_Article::findById($item['id'], $language->getId())) {
							$output .= htmlspecialchars('<xhtml:link rel="alternate" hreflang="'.$language->getLocale().'" href="'.sly_Util_HTTP::getAbsoluteUrl($item['id'], $language->getId()).'" />');
						}
					}

				}
				$output .= '</url>';
			}
		}

		$output .= '</urlset>';

		$response = new sly_Response($output);
		$response->setContentType('text/xml', 'UTF-8');

		return $response;

	}

	protected function getFrequency($time) {
		$diff = time() - $time;

		if ($diff <= 86400) {
			$freq = 'daily';
		} elseif ($diff <= 604800) {
			$freq = 'weekly';
		} elseif ($diff <= 1814400) {
			$freq = 'monthly';
		} else {
			$freq = 'yearly';
		}

		return $freq;
	}

	protected function getAllArticles() {
		$db     = sly_Core::getContainer()->getPersistence();
		$result = $db->select('article', 'id, clang', 'online="1" AND deleted="0"');

		return $result->all();
	}

	/**
	 * @param sly_Router_Base $router
	 * @return sly_Router_Base
	 */
	public static function addRoutes(sly_Router_Base $router) {

		// default action is indexAction
		$router->prependRoute('/sitemap.xml', array('slycontroller' => 'Sitemap'));
		$router->prependRoute('/:lang/sitemap.xml', array('slycontroller' => 'sitemap'));

		return $router;
	}

}