<?php
/**
 * Copyright, KruseMedien GmbH
 *
 * This file is released under the terms of the MIT license.
 * You can find the complete text  online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

namespace km\seo;

class Layout extends \sly_Layout_XHTML5 {

	protected $article;

	public function __construct(\sly_Model_Article $curArticle) {

		// remember the current article for later
		$this->article = $curArticle;

		// Title
		$title = $curArticle->getMeta('km_seo_alternative_title') ?: $curArticle->getName();
		$this->setTitle($title);

		// Description
		$meta_description = $curArticle->getMeta('km_seo_meta_description');
		$meta_description ? $this->addMeta('description', $meta_description) : null;

		// Keywords
		$meta_keywords = $curArticle->getMeta('km_seo_meta_keywords');
		$meta_keywords ? $this->addMeta('keywords', $meta_keywords) : null;

	}

}
